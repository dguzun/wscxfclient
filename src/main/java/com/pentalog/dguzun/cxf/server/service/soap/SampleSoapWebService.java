package com.pentalog.dguzun.cxf.server.service.soap;

import javax.jws.WebService;

import com.pentalog.dguzun.cxf.client.vo.PersonVO;

@WebService
public interface SampleSoapWebService {
	public PersonVO getDataFromWebService();
}

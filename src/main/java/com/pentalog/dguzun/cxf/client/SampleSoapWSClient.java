package com.pentalog.dguzun.cxf.client;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pentalog.dguzun.cxf.server.service.soap.SampleSoapWebService;

public class SampleSoapWSClient {
	
	public SampleSoapWSClient() {
		
		ClassPathXmlApplicationContext classPathXmlAppContext = new ClassPathXmlApplicationContext("classpath:META-INF/beans.xml");
		classPathXmlAppContext.start();
		
		SampleSoapWebService sampleWebService = (SampleSoapWebService)classPathXmlAppContext.getBean("client");
		
		System.out.println(sampleWebService.getDataFromWebService().getName());
		System.out.println(sampleWebService.getDataFromWebService().getAge());
		System.out.println(sampleWebService.getDataFromWebService().getIsAdmin());
		
		classPathXmlAppContext.close();
	}
	
	public static void main(String[] args){
		new SampleSoapWSClient();
	}
}
